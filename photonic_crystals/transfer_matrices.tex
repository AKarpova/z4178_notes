
\documentclass{dspnotes}

\title{Метод матриц переноса для одномерной фотонной структуры\\
	\Large{конспект лекции М. В. Рыбина 27.09.2018}
}
\author{Гоша Ф.}
\date{\vspace{-3ex}}

\begin{document}
	\maketitle
	
Рассмотрим ``одномерный диэлектрик'', т.е. диэлектрическая проницаемость будет зависеть только от $x$, $\varepsilon = \varepsilon(x)$ (можно представлять, например, плёнку на поверхности) и будем освещать его плоской монохроматической волной. Наша цель -- полностью описать рассеяние такой волны, т.е. найти коэффициенты отражения и пропускания и распределение поля во всём пространстве. В направлении $x$ будем считить среду состоящей из слоёв с постоянной диэлектрической проницаемостью.\\

В направлениях $y$ и $z$ поле -- плоская волна. Это следует, например, из теоремы Блоха 
$\mathbf{E}(\mathbf{r}) = 
\mathbf{U}(\mathbf{r})\exp\del{i\del{k_y y + k_z z}}$,
 где функция $U(\mathbf{r})$ периодична с любым периодом по $y$ и $z$, т.е. является функцией только от $x$. Поскольку $k_y/k_z = const$, можем выбрать систему координат, в которой $k_z = 0$, тогда   
 \begin{gather}
     \mathbf{E} = \mathbf{E}(x)e^{ik_y y}\\
     \mathbf{H} = \mathbf{H}(x)e^{ik_y y}
\end{gather}
Подставляем эти поля в уравнения Максвелла с роторами 

\begin{gather*}
\nabla\times\mathbf{H} = -i\omega\eps\eps_0 \mathbf{E}\\ 
\nabla\times\mathbf{E} = i\omega\mu\mu_0\mathbf{H}
\end{gather*}
\begin{gather}
\nabla\times\mathbf{H} = 
\begin{bmatrix}
\dd_y E_z - \dd_z E_y\\
\dd_z E_x - \dd_x E_z\\
\dd_x E_y - \dd_y E_x
\end{bmatrix} = 
\begin{bmatrix}
ik_y H_z\\
-\od{H_z}{x}\\
\od{H_y}{x} - ik_y H_x
\end{bmatrix} = 
-i\omega\eps_0\eps
\begin{bmatrix}
E_x\\
E_y\\
E_z
\end{bmatrix}\label{rotH}\\
\nabla\times\mathbf{E} = 
\begin{bmatrix}
\dd_y H_z - \dd_z H_y\\
\dd_z H_x - \dd_x H_z\\
\dd_x H_y - \dd_y H_x
\end{bmatrix} = 
\begin{bmatrix}
ik_y E_z\\
-\od{E_z}{x}\\
\od{E_y}{x} - ik_y E_x
\end{bmatrix} = 
i\omega\mu_0\mu
\begin{bmatrix}
H_x\\
H_y\\
H_z
\end{bmatrix}\label{rotE}
\end{gather}

Как видим, система распалась на две независимые части: 
\begin{enumerate}
	\item \emph{TE-поляризация}: компоненты $H_z$, $E_x$, $E_y$ (первые две строки~\eqref{rotH}, третья строка~\eqref{rotE}).

	\item \emph{TM-поляризация}: компоненты $E_z$, $H_x$ и $H_y$. 
\end{enumerate}
\subsection{Матрица переноса для TE-поляризации}
	\begin{gather}
	E_x = -\frac{k_y}{\omega\eps\eps_0}H_z\label{TE1}\\
	E_y = -\frac{i}{\omega\eps\eps_0}\od{H_z}{x}\label{TE2}\\
	\od{E_y}{x} - ik_y E_x = i\omega\mu\mu_0 H_z\label{TE3}.
	\end{gather} 
	Дифференцируем \eqref{TE3} по $x$ и подставляем $E_y$ из \eqref{TE2}:
	\begin{equation}\label{dHz}
	\od[2]{H_z}{x} + \del{k_y^2 - \frac{\omega^2\mu\eps}{c^2}}H_z = 0.
	\end{equation}	
	Общее решение \eqref{dHz}:
	\begin{equation}\label{HplusHminus}
	H_z(x) = A_{+}e^{ik_x x} + A_{-}e^{-ik_x x},
	\text{ где }
	k_x(\eps) = \sqrt{\frac{\omega^2\mu\eps}{c^2} - k_y^2}
	\end{equation}
    Отметим, что при нормальном падении $k_x = \omega n / c$.
    Будем обозначать $H_{+} = A_{+}e^{ik_x x}$, $H_{-} = A_{-}e^{-ik_x x}$. Компоненты $H_+$ и $H_-$, будучи плоскими волнами, просто преобразуются вдоль пути внутри однородного диэлектрика: 
    \begin{equation}
    \begin{bmatrix}
    H_+(x+l)\\
    H_-(x+l)
    \end{bmatrix} = 
    \begin{bmatrix}
    e^{ikl} & 0\\
    0 & e^{-ikl}
    \end{bmatrix}
    \begin{bmatrix}
    H_+(x)\\
    H_-(x)
    \end{bmatrix}
     = T 
     \begin{bmatrix}
     H_+(x)\\
     H_-(x)
     \end{bmatrix}
    \end{equation}
	
Поскольку $E_x \propto H_z$, независимых компонент в TE волне только две, выберем 
$E = E_y$, $H = H_z$. Из \eqref{TE2} с учётом~\eqref{HplusHminus} 
\begin{equation*}
	D = \eps\eps_0E = - \frac{i}{\omega}\od{H}{x} = \frac{k}{\omega}H_+ - \frac{k}{\omega}H_-
\end{equation*}
или
\begin{equation}
	\begin{bmatrix}
		D \\
		H
	\end{bmatrix} = 
	\begin{bmatrix}
		k_x/\omega & -k_x/\omega \\
		1 & 1
	\end{bmatrix}
	\begin{bmatrix}
		H_{+} \\
		H_{-}
	\end{bmatrix}
	= A
	 \begin{bmatrix}
		 H_{+} \\
		 H_{-}
	\end{bmatrix}
\end{equation}

Как известно, для матрицы
$M = \del{\begin{smallmatrix}a & b \\ c & d\end{smallmatrix}}$ 
обратная равна $1/\det(M)\cdot \del{\begin{smallmatrix}d & -b \\ -c & a\end{smallmatrix}}$, так что

\begin{gather}
A^{-1} = \frac{1}{2}
\frac{\omega}{k_x}
\begin{bmatrix}
	1 & k_x/\omega\\
	-1 & k_x/\omega
\end{bmatrix}
= \frac{1}{2}
\begin{bmatrix}
\omega/k_x & 1\\
-\omega/k_x & 1
\end{bmatrix}\\
\begin{bmatrix}
H_{+} \\
H_{-}
\end{bmatrix}
= A^{-1}
\begin{bmatrix}
D \\
H
\end{bmatrix}
\end{gather}

Таким образом, величины $D$ и $H$ вутри одного слоя преобразуются как
\begin{equation}
\begin{bmatrix}
D(x+l)\\
H(x+l)
\end{bmatrix}
 = ATA^{-1}
 \begin{bmatrix}
 D(x)\\
 H(x)
 \end{bmatrix}
\end{equation}
На границе слоёв постоянно $E$, а не $D$, поэтому всё запишем относительно $E$:
\begin{equation*}
P = 
\begin{bmatrix}
\frac{1}{\eps\eps_0} & 0\\
0 & 1
\end{bmatrix};\ \  
\begin{bmatrix}E\\ H\end{bmatrix} = P \begin{bmatrix}D\\ H\end{bmatrix};\ \ 
\begin{bmatrix}D\\ H\end{bmatrix} = P^{-1} \begin{bmatrix}E\\ H\end{bmatrix}
\end{equation*}
\begin{equation}
\begin{bmatrix}
E(x+l)\\
H(x+l)
\end{bmatrix}
= PATA^{-1}P^{-1}
\begin{bmatrix}
E(x)\\
H(x)
\end{bmatrix}
\end{equation}
Общая матрица перехода
\begin{equation}
\begin{split}
M &= 
 \frac{1}{2}
% P
\begin{bmatrix}
	\frac{1}{\eps\eps_0} & 0\\
	0 & 1
\end{bmatrix}
% A
\begin{bmatrix}
	\frac{k_x}{\omega} & -\frac{k_x}{\omega} \\
	1 & 1
\end{bmatrix}   
% T
\begin{bmatrix}
	e^{ik_xl} & 0\\
	0 & e^{-ik_xl}
\end{bmatrix}
% A^{-1}
\begin{bmatrix}
	\frac{\omega}{k_x} & 1\\
	-\frac{\omega}{k_x} & 1
\end{bmatrix} 
% P^{-1}
\begin{bmatrix}
	\eps\eps_0 & 0\\
	0 & 1
\end{bmatrix} \\ 
&= \begin{bmatrix}
	\cos\del{k_x l} & i\frac{k_x}{\omega}\frac{1}{\eps\eps_0}\sin\del{k_x l}\\
	i\frac{\omega}{k_x}\eps\eps_0\sin\del{k_x l} & \cos\del{k_x l}
\end{bmatrix}  
\end{split}
\end{equation}

\end{document}