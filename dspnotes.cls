\ProvidesClass{dspnotes}

\LoadClass[11pt, a4paper]{scrartcl}

\usepackage[no-math]{fontspec}
\usepackage{mathpazo}
%\RequirePackage{unicode-math}

\defaultfontfeatures{Ligatures=TeX, Extension=.otf}
\setmainfont[
	BoldFont=FiraSans-Heavy,
	ItalicFont=FiraSans-BookItalic,
	BoldItalicFont=FiraSans-SemiBoldItalic,
]{Heuristica-Regular}
%\setmathfont{xits-math.otf}


\setsansfont[
	BoldFont=cmunsx,
	ItalicFont=cmunsi,
]{cmunss}


%\addtokomafont{disposition}{\rmfamily}


\usepackage[russian,english]{babel}

\usepackage[top=1.5cm, bottom=2cm, left=2.5cm, right=1.5cm]{geometry}
\usepackage[protrusion=true,expansion=true]{microtype}  
%%% Custom headers/footers (fancyhdr package)
\usepackage{fancyhdr}
\pagestyle{fancyplain}
\fancyhead{}                                            % No page header
\fancyfoot[L]{}                                         % Empty 
\fancyfoot[C]{}                                         % Empty
\fancyfoot[R]{\thepage}                                 % Pagenumbering
\renewcommand{\headrulewidth}{0pt}          % Remove header underlines
\renewcommand{\footrulewidth}{0pt}              % Remove footer underlines
\setlength{\headheight}{13.6pt}


\usepackage{mathtools}
\usepackage{bbold} % \mathbb для строчных букв
\usepackage{cmll}  % \wedge
\usepackage{commath}


\usepackage{url}


\newcommand{\R}{\mathbb{R}}
\newcommand{\e}{\mathbb{e}}
\newcommand{\dd}{\partial}
\newcommand{\eps}{\varepsilon}
